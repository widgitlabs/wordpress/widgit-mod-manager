# Mod Manager

This folder contains all the unit tests for Mod Manager.

For more information on how to write PHPUnit Tests,
see [PHPUnit's Website](http://www.phpunit.de/manual/3.6/en/writing-tests-for-phpunit.html).
