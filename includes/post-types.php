<?php
/**
 * Post types
 *
 * @package     Widgit\Mod_Manager\PostTypes
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Register our custom post types
 *
 * @since       1.0.0
 * @return      void
 */
function widgit_mod_manager_add_post_types() {
	$mod_labels = apply_filters(
		'widgit_mod_manager_mod_labels',
		array(
			'name'                  => _x( 'Mods', 'mod post type name', 'widgit-mod-manager' ),
			'singular_name'         => _x( 'Mod', 'singular mod post type name', 'widgit-mod-manager' ),
			'add_new'               => __( 'Add New', 'widgit-mod-manager' ),
			'add_new_item'          => __( 'Add New Mod', 'widgit-mod-manager' ),
			'edit_item'             => __( 'Edit Mod', 'widgit-mod-manager' ),
			'new_item'              => __( 'New Mod', 'widgit-mod-manager' ),
			'all_items'             => __( 'All Mods', 'widgit-mod-manager' ),
			'view_item'             => __( 'View Mod', 'widgit-mod-manager' ),
			'search_items'          => __( 'Search Mods', 'widgit-mod-manager' ),
			'not_found'             => __( 'No mods found', 'widgit-mod-manager' ),
			'not_found_in_trash'    => __( 'No mods found in Trash', 'widgit-mod-manager' ),
			'parent_item_colon'     => '',
			'menu_name'             => _x( 'Mod Manager', 'mod post type menu name', 'widgit-mod-manager' ),
			'featured_image'        => __( 'Mod Image', 'widgit-mod-manager' ),
			'set_featured_image'    => __( 'Set Mod Image', 'widgit-mod-manager' ),
			'remove_featured_image' => __( 'Remove Featured Image', 'widgit-mod-manager' ),
			'use_featured_image'    => __( 'Use as Mod Image', 'widgit-mod-manager' ),
			'attributes'            => __( 'Mod Attributes', 'widgit-mod-manager' ),
			'filter_items_list'     => __( 'Filter mods list', 'widgit-mod-manager' ),
			'items_list_navigation' => __( 'Mods list navigation', 'widgit-mod-manager' ),
			'item_list'             => __( 'Mods list', 'widgit-mod-manager' ),
		)
	);

	$mod_args = array(
		'labels'             => $mod_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'menu_position'      => 30,
		'menu_icon'          => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyNi4wLjMsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAxMTIuNSAxMDUuMyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTEyLjUgMTA1LjM7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnIGlkPSJsb2dvX2ljb25fMl8iPg0KCTxwb2x5Z29uIGlkPSJzaGFwZV83XyIgcG9pbnRzPSIxMTIuNSwxMDEuMyAxMTIuNSw2MC4yIDkyLDgwLjggCSIvPg0KCTxwb2x5Z29uIGlkPSJzaGFwZV82XyIgcG9pbnRzPSI4Ni40LDc1LjEgMTEyLjUsNDkgMTEyLjUsMCA2MS45LDUwLjYgCSIvPg0KCTxwb2x5Z29uIGlkPSJzaGFwZV81XyIgcG9pbnRzPSIyMC41LDgwLjggMCw2MC4yIDAsMTAxLjMgCSIvPg0KCTxwb2x5Z29uIGlkPSJzaGFwZV80XyIgcG9pbnRzPSI4MC44LDgwLjggMCwwIDAsNDkgNTYuMywxMDUuMyAJIi8+DQo8L2c+DQo8L3N2Zz4NCg==',
		'rewrite'            => array(
			'slug'       => 'mod',
			'with_front' => false,
		),
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		'show_in_rest'       => true,
		'supports'           => apply_filters( 'widgit_mod_manager_mod_supports', array( 'title', 'editor', 'thumbnail', 'revision', 'page-attributes' ) ),
	);
	register_post_type( 'mod', apply_filters( 'widgit_mod_manager_mod_post_type_args', $mod_args ) );
}
add_action( 'init', 'widgit_mod_manager_add_post_types', 1 );


/**
 * Change default "Enter title here" input
 *
 * @since       1.0.0
 * @param       string $title The existing title.
 * @return      string $title The updated title.
 */
function widgit_mod_manager_change_default_title( $title ) {
	$screen = get_current_screen();

	if ( 'mod' === $screen->post_type ) {
		$title = __( 'Enter mod name here', 'widgit-mod-manager' );
	}

	return $title;
}
add_filter( 'enter_title_here', 'widgit_mod_manager_change_default_title' );


/**
 * Register the custom taxonomies for our post types
 *
 * @since       1.0.0
 * @return      void
 */
function widgit_mod_manager_setup_mod_taxonomies() {
	$category_labels = array(
		'name'              => _x( 'Mod Categories', 'taxonomy general name', 'widgit-mod-manager' ),
		'singular_name'     => _x( 'Mod Category', 'taxonomy singular name', 'widgit-mod-manager' ),
		'search_items'      => __( 'Search Mod Categories', 'widgit-mod-manager' ),
		'all_items'         => __( 'All Mod Categories', 'widgit-mod-manager' ),
		'parent_item'       => __( 'Parent Mod Category', 'widgit-mod-manager' ),
		'parent_item_colon' => __( 'Parent Mod Category:', 'widgit-mod-manager' ),
		'edit_item'         => __( 'Edit Mod Category', 'widgit-mod-manager' ),
		'update_item'       => __( 'Update Mod Category', 'widgit-mod-manager' ),
		'add_new_item'      => __( 'Add New Mod Category', 'widgit-mod-manager' ),
		'new_item_name'     => __( 'New Mod Category Name', 'widgit-mod-manager' ),
		'not_found'         => __( 'No Mod Categories found', 'widgit-mod-manager' ),
		'menu_name'         => __( 'Categories', 'widgit-mod-manager' ),
	);

	$category_args = apply_filters(
		'widgit_mod_manager_mod_category_args',
		array(
			'hierarchical' => true,
			'labels'       => apply_filters( 'widgit_mod_manager_mod_category_labels', $category_labels ),
			'show_ui'      => true,
			'query_var'    => 'mod_category',
			'rewrite'      => array(
				'slug'         => 'mods/category',
				'with_front'   => false,
				'hierarchical' => true,
			),
		)
	);
	register_taxonomy( 'mod_category', array( 'mod' ), $category_args );
	register_taxonomy_for_object_type( 'mod_category', 'mod' );

	$tag_labels = array(
		'name'                  => _x( 'Mod Tags', 'taxonomy general name', 'widgit-mod-manager' ),
		'singular_name'         => _x( 'Mod Tag', 'taxonomy singular name', 'widgit-mod-manager' ),
		'search_items'          => __( 'Search Mod Tags', 'widgit-mod-manager' ),
		'all_items'             => __( 'All Mod Tags', 'widgit-mod-manager' ),
		'parent_item'           => __( 'Parent Mod Tag', 'widgit-mod-manager' ),
		'parent_item_colon'     => __( 'Parent Mod Tag:', 'widgit-mod-manager' ),
		'edit_item'             => __( 'Edit Mod Tag', 'widgit-mod-manager' ),
		'update_item'           => __( 'Update Mod Tag', 'widgit-mod-manager' ),
		'add_new_item'          => __( 'Add New Mod Tag', 'widgit-mod-manager' ),
		'new_item_name'         => __( 'New Mod Tag Name', 'widgit-mod-manager' ),
		'not_found'             => __( 'No Mod Tags found', 'widgit-mod-manager' ),
		'menu_name'             => __( 'Tags', 'widgit-mod-manager' ),
		'choose_from_most_used' => __( 'Choose from most used mod tags', 'widgit-mod-manager' ),
	);

	$tag_args = apply_filters(
		'widgit_mod_manager_mod_tag_args',
		array(
			'hierarchical' => false,
			'labels'       => apply_filters( 'widgit_mod_manager_mod_tag_labels', $tag_labels ),
			'show_ui'      => true,
			'query_var'    => 'mod_tag',
			'rewrite'      => array(
				'slug'         => 'mods/tag',
				'with_front'   => false,
				'hierarchical' => true,
			),
		)
	);
	register_taxonomy( 'mod_tag', array( 'mod' ), $tag_args );
	register_taxonomy_for_object_type( 'mod_tag', 'mod' );
}
add_action( 'init', 'widgit_mod_manager_setup_mod_taxonomies', 0 );


/**
 * Updated messages
 *
 * @since       1.0.0
 * @param       array $messages Post Updated messages.
 * @return      array $messages Updated Post Updated messages.
 */
function widgit_mod_manager_updated_messages( $messages ) {
	global $post, $post_ID;

	$messages['mod'] = array(
		1 => __( 'Mod updated.', 'widgit-mod-manager' ),
		4 => __( 'Mod updated.', 'widgit-mod-manager' ),
		6 => __( 'Mod published.', 'widgit-mod-manager' ),
		7 => __( 'Mod saved.', 'widgit-mod-manager' ),
		8 => __( 'Mod submitted.', 'widgit-mod-manager' ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'widgit_mod_manager_updated_messages' );



/**
 * Updated bulk messages
 *
 * @since       1.0.0
 * @param       array $bulk_messages Post Updated messages.
 * @param       array $bulk_counts The post counts.
 * @return      array $bulk_messages Updated Post Updated messages.
 */
function widgit_mod_manager_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	$singular = 'Mod';
	$plural   = 'Mods';

	$bulk_messages['mod'] = array(
		// Translators: placeholder represents count.
		'updated'   => _n( '%s mod updated.', '%s mods updated.', $bulk_counts['updated'], 'widgit-mod-manager' ),
		// Translators: placeholder represents count.
		'locked'    => _n( '%s mod not updated, somebody is editing it.', '%s mods not updated, somebody is editing them.', $bulk_counts['locked'], 'widgit-mod-manager' ),
		// Translators: placeholder represents count.
		'deleted'   => _n( '%s mod permanently deleted.', '%s mods permanently deleted.', $bulk_counts['deleted'], 'widgit-mod-manager' ),
		// Translators: placeholder represents count.
		'trashed'   => _n( '%s mod moved to the Trash.', '%s mods moved to the Trash.', $bulk_counts['trashed'], 'widgit-mod-manager' ),
		// Translators: placeholder represents counts.
		'untrashed' => _n( '%s mod restored from the Trash.', '%s restored from the Trash.', $bulk_counts['untrashed'], 'widgit-mod-manager' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'widgit_mod_manager_bulk_updated_messages', 10, 2 );


/**
 * Add row actions for our post types
 *
 * @since       1.0.0
 * @param       array  $actions The existing actions.
 * @param       object $post The post object.
 * @return      array  $actions The updated post actions.
 */
function widgit_mod_manager_mod_row_actions( $actions, $post ) {
	if ( 'mod' === $post->post_type ) {
		$actions = array_merge( array( 'id' => 'ID: ' . $post->ID ), $actions );
	}

	return $actions;
}
add_filter( 'post_row_actions', 'widgit_mod_manager_mod_row_actions', 2, 100 );
