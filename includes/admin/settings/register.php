<?php
/**
 * Register settings
 *
 * @package     Widgit\Mod_Manager\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings.
 */
function widgit_mod_manager_add_menu( $menu ) {
	// TODO: This needs to be swappable depending on number of plugins installed.
	$menu['type']       = 'submenu';
	$menu['parent']     = 'edit.php?post_type=mod';
	$menu['page_title'] = __( 'Mod Manager', 'widgit-mod-manager' );
	$menu['menu_title'] = __( 'Settings', 'widgit-mod-manager' );
	$menu['show_title'] = true;

	return $menu;
}
add_filter( 'widgit_mod_manager_menu', 'widgit_mod_manager_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs.
 */
function widgit_mod_manager_settings_tabs( $tabs ) {
	$tabs['general'] = __( 'General', 'widgit-mod-manager' );
	$tabs['support'] = __( 'Support', 'widgit-mod-manager' );

	return $tabs;
}
add_filter( 'widgit_mod_manager_settings_tabs', 'widgit_mod_manager_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections.
 */
function widgit_mod_manager_registered_settings_sections( $sections ) {
	// TODO: Make Welcome! only show on initial install and updates.
	$sections = array(
		'general' => array(
			'mod-manager'  => __( 'Mod Manager', 'widgit-mod-manager' ),
			'text-changes' => __( 'Text Changes', 'widgit-mod-manager' ),
		),
		'support' => array(),
	);

	return $sections;
}
add_filter( 'widgit_mod_manager_registered_settings_sections', 'widgit_mod_manager_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function widgit_mod_manager_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'widgit_mod_manager_unsavable_tabs', 'widgit_mod_manager_define_unsavable_tabs' );


/**
 * Disable save button on unsavable sections
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function widgit_mod_manager_define_unsavable_sections() {
	$sections = array( 'core/welcome' );

	return $sections;
}
add_filter( 'widgit_mod_manager_unsavable_sections', 'widgit_mod_manager_define_unsavable_sections' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function widgit_mod_manager_registered_settings( $settings ) {
	$general_settings = array(
		'general' => apply_filters(
			'widgit_mod_manager_registered_settings_general',
			array(
				'mod-manager'  => array(
					array(
						'id'   => 'mod_manager_header',
						'name' => '<h2>' . __( 'Mod Manager', 'widgit-mod-manager' ) . '</h2>',
						'desc' => '',
						'type' => 'header',
					),
				),
				'text-changes' => array(
					array(
						'id'   => 'text_changes_header',
						'name' => '<h2>' . __( 'Text Changes', 'widgit-mod-manager' ),
						'desc' => '',
						'type' => 'header',
					),
				),
			),
		),
	);

	$support_settings = array(
		'support' => array(
			array(
				'id'   => 'support_header',
				'name' => __( 'Mod Manager Support', 'widgit-mod-manager' ),
				'desc' => '',
				'type' => 'header',
			),
			array(
				'id'   => 'system_info',
				'name' => __( 'System Info', 'widgit-mod-manager' ),
				'desc' => '',
				'type' => 'sysinfo',
			),
		),
	);

	return array_merge( $general_settings, $support_settings );
}
add_filter( 'widgit_mod_manager_registered_settings', 'widgit_mod_manager_registered_settings' );
