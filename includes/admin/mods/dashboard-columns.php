<?php
/**
 * Mod dashboard columns
 *
 * @package     Widgit\Mod_Manager\Mods\Dashboard_Columns
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Set up the custom mod columns
 *
 * @since       1.0.0
 * @param       array $columns The mod dashboard columns.
 * @return      array $columns The mod dashboard columns.
 */
function widgit_mod_manager_mod_columns( $columns ) {
	$columns = array(
		'cb'           => '<input type="checkbox"/>',
		'title'        => __( 'Name', 'widgit-mod-manager' ),
		'mod_category' => __( 'Categories', 'widgit-mod-manager' ),
		'mod_tag'      => __( 'Tags', 'widgit-mod-manager' ),
		'date'         => __( 'Date Added', 'widgit-mod-manager' ),
	);

	return apply_filters( 'widgit_mod_manager_mod_columns', $columns );
}
add_filter( 'manage_edit-mod_columns', 'widgit_mod_manager_mod_columns' );


/**
 * Render download column content
 *
 * @since       1.0.0
 * @param       string $column_name Column name.
 * @param       int    $post_id Post ID.
 * @return      void
 */
function widgit_mod_manager_manage_posts_custom_column( $column_name, $post_id ) {
	if ( 'mod' === get_post_type( $post_id ) ) {
		switch ( $column_name ) {
			case 'mod_category':
				echo get_the_term_list( $post_id, 'mod_category', '', ', ', '' );
				break;
			case 'download_tag':
				echo get_the_term_list( $post_id, 'mod_tag', '', ', ', '' );
				break;
		}
	}
}
add_action( 'manage_posts_custom_column', 'widgit_mod_manager_manage_posts_custom_column', 10, 2 );


/**
 * Add mod filters
 *
 * @since       1.0.0
 * @return      void
 */
function widgit_mod_manager_add_mod_filters() {
	if ( isset( $_REQUEST['widgit_mod_manager_nonce'] ) ) {
		check_admin_referer( 'widgit_mod_manager_nonce', 'widgit_mod_manager_nonce' );
	}

	global $typenow;

	if ( 'mod' === $typenow ) {
		$get   = wp_unslash( $_GET );
		$terms = get_terms( 'mod_category' );

		if ( count( $terms ) > 0 ) {
			echo '<select name="mod_category" id="mod_category" class="postform">';
			echo '<option value="">' . esc_html__( 'Show all categories', 'widgit-mod-manager' ) . '</option>';

			foreach ( $terms as $term ) {
				$selected = isset( $get['mod_category'] ) && $get['mod_category'] == $term->slug ? ' selected="selected"' : '';

				echo '<option value="' . esc_attr( $term->slug ) . '"' . esc_html( $selected ) . '>' . esc_html( $term->name ) . ' (' . esc_html( $term->count ) . ')</option>';
			}

			echo '</select>';
		}

		$terms = get_terms( 'mod_tag' );

		if ( count( $terms ) > 0 ) {
			echo '<select name="mod_tag" id="mod_tag" class="postform">';
			echo '<option value="">' . esc_html__( 'Show all tags', 'widgit-mod-manager' ) . '</option>';

			foreach ( $terms as $term ) {
				$selected = isset( $get['mod_tag'] ) && $get['mod_tag'] == $term->slug ? ' selected="selected"' : '';

				echo '<option value="' . esc_attr( $term->slug ) . '"' . esc_html( $selected ) . '>' . esc_html( $term->name ) . ' (' . esc_html( $term->count ) . ')</option>';
			}

			echo '</select>';
		}
	}

}
add_action( 'restrict_manage_posts', 'widgit_mod_manager_add_mod_filters', 100 );

/**
 * Remove month filter
 *
 * @since       1.0.0
 * @param       array $dates The preset array of dates.
 * @return      array Empty array disables the dropdown
 */
function widgit_mod_manager_remove_month_filter( $dates ) {
	global $typenow;

	if ( 'mod' === $typenow ) {
		$dates = array();
	}

	return $dates;
}
add_filter( 'months_dropdown_results', 'widgit_mod_manager_remove_month_filter', 99 );
