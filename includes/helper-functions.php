<?php
/**
 * Helper functions
 *
 * @package     Widgit\Mod_Manager\Functions
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
