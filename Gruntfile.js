/*global console, module, require*/
module.exports = function (grunt) {
	// Load multiple grunt tasks using globbing patterns
	require('load-grunt-tasks')(grunt);

	// Project configuration.
	grunt.initConfig({
	  pkg: grunt.file.readJSON('package.json'),

	  // Run JSHint
	  jshint: {
		options: {
		  jshintrc: '.jshintrc',
		},
		with_overrides: {
		  files: {
			src: ['**/*.js'],
		  },
		},
	  },

	  // Run StyleLint
	  stylelint: {
		files: '**/*.css',
	  },

	  // Run MarkdownLint
	  markdownlint: {
		full: {
		  options: {
			config: {
			  default: true,
			},
		  },
		  src: [
			'**/*.md',
			'!out/**/*.md',
			'!node_modules/**/*.md',
			'!vendor/**/*.md',
			'!**/CHANGELOG.md',
		  ],
		},
		changelog: {
		  options: {
			config: {
			  default: true,
			  MD022: false,
			  MD024: false,
			  MD032: false,
			},
		  },
		  src: [
			'**/CHANGELOG.md',
			'!out/**/*.md',
			'!node_modules/**/*.md',
			'!vendor/**/*.md',
		  ],
		},
	  },

	  // Run PHPCS and php -l
	  exec: {
		phpsyntax: {
		  cmd:
			'find -L . -not -path \'./vendor/*\' -not -path \'./node_modules/*\' -name \'*.php\' -print0 | xargs -0 -n 1 -P 4 php -l',
		},
		phpcs: {
		  cmd:
			'./vendor/bin/phpcs . -p --standard=WordPress-Extra,WordPress-Docs,WordPress-VIP-Go,PHPCompatibilityWP --extensions=php --warning-severity=8 --error-severity=1 --ignore=vendor,node_modules',
		},
	  },

	  // Validate textdomains
	  checktextdomain: {
		options: {
		  text_domain: ['widgit-mod-manager'],
		  keywords: [
			'__:1,2d',
			'_e:1,2d',
			'_x:1,2c,3d',
			'esc_html__:1,2d',
			'esc_html_e:1,2d',
			'esc_html_x:1,2c,3d',
			'esc_attr__:1,2d',
			'esc_attr_e:1,2d',
			'esc_attr_x:1,2c,3d',
			'_ex:1,2c,3d',
			'_n:1,2,3,4d',
			'_nx:1,2,4c,5d',
			'_n_noop:1,2,3d',
			'_nx_noop:1,2,3c,4d',
			' __ngettext:1,2,3d',
			'__ngettext_noop:1,2,3d',
			'_c:1,2d',
			'_nc:1,2,4c,5d',
		  ],
		},
		files: {
		  src: [
			'**/*.php', // Include all files
			'!node_modules/**', // Exclude node_modules/
			'!out/**', // Exclude out/
			'!includes/libraries/**', // Exclude libraries
			'!vendor/**', // Exclude vendor
		  ],
		  expand: true,
		},
	  },

	  // Minify CSS
	  cssmin: {
		options: {
		  mergeIntoShorthands: false,
		},
		target: {
		  files: [
			{
			  expand: true,
			  cwd: 'assets/css',
			  src: ['*.css', '!select2*.css'],
			  dest: 'assets/css',
			  ext: '.min.css',
			},
		  ],
		},
	  },

	  // Minify JS
	  uglify: {
		options: {
		  mangle: false,
		},
		target: {
		  files: [
			{
			  expand: true,
			  cwd: 'assets/js',
			  src: ['*.js', '!*.min.js', '!*jquery*.js', '!select2*.js'],
			  dest: 'assets/js',
			  ext: '.min.js',
			  extDot: 'last',
			},
		  ],
		},
	  },

	  // Update pot files
	  makepot: {
		target: {
		  options: {
			domainPath: '/languages/', // Where to save the POT file.
			exclude: ['out/.*', 'includes/libraries/.*'],
			mainFile: 'class-widgit-mod-manager.php', // Main project file.
			potFilename: 'widgit-mod-manager.pot', // Name of the POT file.
			potHeaders: {
			  poedit: true, // Includes common Poedit headers.
			  'x-poedit-keywordslist': true, // Include a list of all possible gettext functions.
			},
			type: 'wp-plugin', // Type of project (wp-plugin or wp-theme).
			updateTimestamp: true, // Whether the POT-Creation-Date should be updated without other changes.
			processPot: function (pot) {
			  pot.headers['report-msgid-bugs-to'] =
				'https://gitlab.com/widgitlabs/wordpress/widgit-mod-manager/issues';
			  pot.headers['last-translator'] =
				'Daniel J Griffiths (https://evertiro.com/)';
			  pot.headers['language-team'] = 'Widgit Labs <support@widgit.io>';
			  pot.headers.language = 'en_US';
			  var translation, // Exclude meta data from pot.
				excluded_meta = [
				  'Plugin Name of the plugin/theme',
				  'Plugin URI of the plugin/theme',
				  'Author of the plugin/theme',
				  'Author URI of the plugin/theme',
				];
			  for (translation in pot.translations['']) {
				if (
				  'undefined' !==
				  typeof pot.translations[''][translation].comments.extracted
				) {
				  if (
					excluded_meta.indexOf(
					  pot.translations[''][translation].comments.extracted
					) >= 0
				  ) {
					console.log(
					  'Excluded meta: ' +
						pot.translations[''][translation].comments.extracted
					);
					delete pot.translations[''][translation];
				  }
				}
			  }
			  return pot;
			},
		  },
		},
	  },
	});

	// Build task(s).
	grunt.registerTask('lint_jshint', ['jshint']);
	grunt.registerTask('lint_stylelint', ['stylelint']);
	grunt.registerTask('lint_markdownlint', ['markdownlint']);

	grunt.registerTask('standards_phpsyntax', ['exec:phpsyntax']);
	grunt.registerTask('standards_phpcs', ['exec:phpcs']);
	grunt.registerTask('standards_i18n', ['checktextdomain']);

	grunt.registerTask('test', [
	  'lint_jshint',
	  'lint_stylelint',
	  'lint_markdownlint',
	  'standards_phpsyntax',
	  'standards_phpcs',
	  'standards_i18n',
	]);

	grunt.registerTask('stage', ['cssmin', 'uglify', 'makepot']);

	grunt.registerTask('build', ['test', 'stage']);
  };
