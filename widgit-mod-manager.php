<?php
/**
 * Plugin Name:     Widgit Mod Manager
 * Plugin URI:      https://widgit.io
 * Description:     A mod manager for modding guides
 * Author:          Widgit Team
 * Author URI:      https://widgit.io
 * Version:         1.0.0
 * Text Domain:     widgit-mod-manager
 * Domain Path:     languages
 *
 * @package         Widgit\Mod_Manager
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

